function cacheFunction(cb){

    if (typeof cb !== 'function'){
        console.error("Enter a valid callback");
        return function(){};
    }

    let cache = {};

    return function(arg){
        if (cache[arg] === "visited"){
            console.log(cache[arg]);
        }
        else{
            cache[arg] = "visited";
            cb(arg);
        }
    }
}

module.exports = {
    cacheFunction
}
