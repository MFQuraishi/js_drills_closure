function limitFunctionCall(n, cb){
    let count = 0;

    if (typeof cb !== 'function' || typeof n !== 'number'){
        console.error("enter valid arguments");
        return ()=>{};       
    }
    
    return function(){
        if(count < n){
            count += 1;
            cb(count);
        }
        else{
            return null;
        }
    }
}

module.exports = {
    limitFunctionCall
}