const {limitFunctionCall} = require('./../limitFunctionCallCount.js');

let func = limitFunctionCall(5, (n)=>{console.log("call back called: "+n)});

func();
func();
func();
func();
func();
func();
func();