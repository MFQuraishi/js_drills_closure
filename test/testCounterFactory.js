const counterFactory = require('./../counterFactory');

const count = counterFactory();

console.log(count.increment());
console.log(count.increment());
console.log(count.increment());

console.log(count.decrement());
console.log(count.decrement());
